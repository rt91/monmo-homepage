/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				'primary': '#3D3D3D',
				'secondary': '#7D41FF',
			},
			fontFamily: {
				sans: ['Montserrat', ...defaultTheme.fontFamily.sans],
			},
		},
	},
	plugins: [],
}

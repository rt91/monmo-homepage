function clx(...args: string[]) {
  return args.filter(Boolean).join(' ')
}

export { clx }
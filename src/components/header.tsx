interface HeaderProps {
    className?: string;
}

const Header = ({ className }: HeaderProps) => {
    return (
        <div className={`flex flex-wrap justify-center items-center space-x-0 sm:space-x-20 space-y-24 ${className}`} role="banner">
            <img className="h-[350px] w-[350px] sm:h-[450px] max-w-none sm:w-[450px] drop-shadow-2xl" src="/assets/monmo_app_icon.png" alt="Logo of the Monmo app" />
            <div className="flex flex-col space-y-8">
                <h1 className="text-white text-4xl sm:text-7xl font-thin">Wait for your moments and more in a better way with <span className="font-light">Monmo.</span></h1>
                <h2 className="text-white text-xl sm:text-2xl font-thin">Made with ❤️ and <span className="font-light">passion.</span></h2>
            </div>
        </div>
    );
}

export default Header;

interface ImageRowProps {
    className?: string;
}

const images = ["/assets/1_light.png", "/assets/2_light.png", "/assets/3_light.png", "/assets/1_dark.png", "/assets/2_dark.png", "/assets/3_dark.png"];

const ImageRow = ({ className }: ImageRowProps) => {
    return (
        <div role="group" className={`flex flex-wrap justify-center items-center gap-20 mx-20 ${className}`} aria-label="image gallery">
            {
                images.map((image, index) => (
                    <img
                        key={index}
                        className="w-[280px] drop-shadow-2xl"
                        src={image}
                        alt={`Detailed description of image ${index + 1}`}
                    />
                ))
            }
        </div>
    );
}

export default ImageRow;
